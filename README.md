# Shoppingkart PHP

Just a development fast test about build an internal system for a Shopping Cart based in PHP.
**Time:** 4 hours. 

## Author
* David Rodríguez, [@davidjguru](https://twitter.com/davidjguru)  
* davidjguru@gmail.com  
* [davidjguru.github.io](https://davidjguru.github.io)


## Notes about business rules
Implementation of business rules 1, 2 and 4; the latter without dependency on the customer group.
Simple concepts of object-oriented programming (e.g. class division, responsibilities, exception handling) were used meaningfully.

## Business rules
1- A discount can be defined as absolute discount or percentage discount.   
2- A discount can be defined for a specific product.    
4- A discount can be applied to the entire shopping cart depending on the customer group.  

## Key Concepts

* Discount: id, nature, value.  
* Product: id, name, price, discount, stock.  
* Customer: id, shopping cart, order.   
* Handlers: Order, Shopping Cart.  
* Test: CartTest.php   

* Architecture: free, pure "vanilla" but using Composer in order to manage dependencies as PHPUnit and the same Composer allow generate a own autoloader for the namespaces, respecting the guidelines of PSR.  
* Language: PHP Version 7.3

## TO*DO 
* Implement the Observer pattern in order to watching all the changes in products.