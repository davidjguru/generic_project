<?php

// TODO - Change this file for a PHPUnit well-formed test.

 $customer = new \generic_project\Customer\CustomerBase('AAAAA');

 // Create some discounts.
  $d1 = new \generic_project\Discount\DiscountBase('1', 'percent', 3.5);
  $d2 = new \generic_project\Discount\DiscountBase('2', 'absolute', 10);

 // Create some products.
 $p1 = new \generic_project\Product\ProductBase('A', '111', 8.0, $d2, 30 );
 $p2 = new \generic_project\Product\ProductBase('B', '222', 23, $d1, 20);
 $p3 = new \generic_project\Product\ProductBase('C', '333', 3.45, $d1, 10);
