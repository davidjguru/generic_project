<?php

namespace generic_project\Customer;

use generic_project\Handlers\ShoppingCart;

/**
 * Defines an interface for customer.
 */
interface CustomerInterface {

  /**
   * Obtains an ID code from a customer.
   *
   * @return string
   *   An unique identifier of a customer.
   */
  public function getCustomerID();

  /**
   * Set an ID code to a customer.
   *
   * @param string $customer_id
   *   An unique identifier for a customer.
   *
   * @return void
   */
  public function setCustomerID($customer_id);

  /**
   * Add a new Item in a shopping cart for a certain customer.
   *
   * @param \generic_project\Product\ProductInterface $product
   *   A new product.
   * @return void
   */
  public function addItemToShoppingCart($product);

  /**
   * @param \generic_project\Product\ProductInterface $product
   *   A product for updating.
   *
   * @param int $qty
   *  Quantity for the product.
   *
   * @return void
   */
  public function updateItemToShoppingCart($product, $qty);

  /**
   * @param \generic_project\Product\ProductInterface $product
   *   A product to delete.
   *
   * @return void
   */
  public function deleteItemFromShoppingCart($product);

  /**
   * Calculate the final value of a Purchase order.
   *
   * @param \generic_project\Handlers\ShoppingCart $customer_shopping_cart
   *
   * @return float
   *   Final value of the purchase.
   */
  public function calculateShoppingCart($customer_shopping_cart);

}