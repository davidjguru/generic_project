<?php

namespace generic_project\Customer;

use generic_project\Handlers\Order;
use generic_project\Handlers\ShoppingCart;


/**
 * Class CustomerBase.
 * Define the base model for a customer profile.
 */
class CustomerBase implements CustomerInterface {

  /**
   * @var string
   */
  protected $customer_id;

  /**
   * @var \generic_project\Handlers\Order
   */
  protected $customer_order;

  /**
   * @var \generic_project\Handlers\ShoppingCart
   */
  protected $customer_shopping_cart;

  /**
   * CustomerBase constructor.
   *
   * @param string $customer_id
   */
  public function __construct(string $customer_id) {

    // Set the initial ID value for each customer.
    $this->customer_id = $customer_id;
    $this->customer_shopping_cart = new ShoppingCart($this->customer_id);
  }

  /**
   * @inheritDoc
   */
  public function getCustomerID()
  {
    return $this->customer_id;
  }

  /**
   * @inheritDoc
   */
  public function setCustomerID($customer_id)
  {
    $this->customer_id = $customer_id;
  }

  /**
   * @inheritDoc
   */
  public function calculateShoppingCart($customer_shopping_cart)
  {
    $this->customer_order = new Order($customer_shopping_cart);
    $final_value = $this->customer_order->createOrder();

    return $final_value;
  }

  /**
   * @inheritDoc
   */
  public function addItemToShoppingCart($product)
  {
    $this->customer_shopping_cart->addItem($product);
  }

  /**
   * @inheritDoc
   */
  public function updateItemToShoppingCart($product, $qty)
  {
    $this->customer_shopping_cart->updateItem($product, $qty);
  }

  /**
   * @inheritDoc
   */
  public function deleteItemFromShoppingCart($product)
  {
    $this->customer_shopping_cart->deleteItem($product);
  }
}