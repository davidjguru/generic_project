<?php

namespace generic_project\Handlers;

use Exception;
use generic_project\Handlers\ShoppingCart;

class Order {

  /**
   * @var \generic_project\Handlers\ShoppingCart $customer_shopping_cart
   */
  protected $customer_shopping_cart;

  /**
   * @var float $order_value
   */
  protected $order_value;

  /**
   * Order Class constructor.
   *
   * @param \generic_project\Handlers\ShoppingCart $customer_shopping_cart
   */
  public function __construct(ShoppingCart $customer_shopping_cart) {
    $this->customer_shopping_cart = $customer_shopping_cart;
    $this->order_value = 0.0;
  }

  /**
   * Returns the final value of a order counting prices by product and discounts.
   *
   * @return float $order_value
   */
  public function createOrder()
  {
    try {

      // Only if the Shopping List is not empty.
      if (!($this->customer_shopping_cart->isEmpty())){

        foreach ($this->customer_shopping_cart as $product) {
                // TODO - Implements the sum of values.
                // Only for fast testing.
               $this->order_value++;
        }
      }
      return $this->order_value;
    } catch (Exception $e) {
            echo 'Error processing the order: ',  $e->getMessage(), "\n";
    }
  }

}
