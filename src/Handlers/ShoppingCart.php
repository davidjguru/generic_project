<?php

namespace generic_project\Handlers;

use generic_project\Product\ProductInterface;

/**
 * Class ShoppingCart.
 * Define the item list of a customer.
 */

class ShoppingCart implements Iterator, Countable {

  /**
   * @var array
   */
  protected $shoppingcart_products = [];

  /**
   * @var int
   */
  protected $position = 0;

  /**
   * @var array
   */
  protected $ids = [];

  /**
   * @var string
   */
  protected $customer_id;

  /**
   * ShoppingCart constructor.
   *
   */
  function __construct(string $customer_id) {
    $this->customer_id = $customer_id;
    $this->shoppingcart_products = [];
    $this->ids = [];
  }

  /**
   * Test if a ShoppingCart is empty.
   *
   */
  public function isEmpty() {
    // Returns Boolean testing if the cart is empty
    return (empty($this->shoppingcart_products));
  }

  /**
   * Adds a new product to the ShoppingCart.
   *
   * @param \generic_project\Product\ProductInterface $product
   *
   */
  public function addItem(ProductInterface $product) {

    // Getting the product id.
    $product_id = $product->getProductID();

    // Throws an exception if there's no id.
    if (!$product_id) throw new Exception('Requires Products with Unique IDs.');

    if (isset($this->shoppingcart_products[$product_id])) {
      $this->updateItem($product, $this->shoppingcart_products[$product]['qty'] + 1);
    } else {
      $this->shoppingcart_products[$product_id] = ['product' => $product, 'qty' => 1];
      // We will save the item id in any case.
      $this->ids[] = $product_id;
    }
  }

  /**
   * Update a product of the ShoppingCart.
   *
   * @param \generic_project\Product\ProductInterface $product
   * @param int $qty
   *
   */
  public function updateItem(ProductInterface $product, $qty) {

    $product_id = $product->getProductID();

    // Delete or update.
    if ($qty === 0) {
      $this->deleteItem($product);
    } elseif ( ($qty > 0) && ($qty != $this->shoppingcart_products[$product_id]['qty'])) {
      $this->shoppingcart_products[$product_id]['qty'] = $qty;
    }

  }

  /**
   * Delete a product from the ShoppingCart.
   *
   * @param \generic_project\Product\ProductInterface $product
   *
   */
  public function deleteItem(ProductInterface $product) {

    $product_id = $product->getProductID();

    // Remove the product.
    if (isset($this->shoppingcart_products[$product_id])) {
      unset($this->shoppingcart_products[$product_id]);

      // Also remove the stored id.
      $shoppingcart_index = array_search($product_id, $this->ids);
      unset($this->ids[$shoppingcart_index]);
    }

  }
  /**
   * Get the current product.
   *
   */
  public function current() {

    $shoppingcart_index = $this->ids[$this->position];
    return $this->shoppingcart_products[$shoppingcart_index];

  }

  /**
   * Get the current position.
   *
   */
  public function key() {
    return $this->position;
  }

  /**
   * Get the next position.
   *
   */
  public function next() {
    $this->position++;
  }

  /**
   * Return to the initial position.
   *
   */
  public function rewind() {
    $this->position = 0;
  }

  /**
   * Test if a product exists.
   *
   */
  public function valid() {
    return (isset($this->ids[$this->position]));
  }

  /**
   * Get the total amount of products in a shopping list.
   *
   */
  public function count() {
    return count($this->shoppingcart_products);
  }
}
