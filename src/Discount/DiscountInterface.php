<?php


namespace generic_project\Discount;

/**
 * Defines an interface for discounts.
 */
interface DiscountInterface {

  /**
   * Get the current ID of the discount.
   *
   * @return string
   *  Current identifier of a discount.
   */
  public function getDiscountID();

  /**
   * Set the current ID of the discount.
   *
   * @param string
   *  New identifier for a discount.
   *
   * @return void
   *
   */
  public function setDiscountID($discount_id);

  /**
   * Get the current nature of the discount (percentage, absolute).
   *
   * @return string
   *  Current nature of a discount.
   */
  public function getNatureOfDiscount();

  /**
   * Set a new nature of the discount (percentage, absolute).
   *
   * @param string $discount_nature
   *   Kind of the discount.
   *
   * @return void
   */
  public function setNatureOfDiscount($discount_nature);

  /**
   * Change the current nature of the discount.
   *
   * @return void
   */
  public function changeNature();

  /**
   * Set a new value of a discount.
   *
   * @param float $discount_value
   *  New value for a discount.
   *
   * @return void
   */
  public function setValue($discount_value);

  /**
   * Get the current value of the discount.
   *
   * @return float
   *  Current value of a discount.
   */
  public function getValue();
}