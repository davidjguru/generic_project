<?php


namespace generic_project\Discount;

/**
 * Class DiscountBase.
 * Define the base model for a single discount.
 */
class DiscountBase implements DiscountInterface {

  /**
   * @var string
   */
  protected $discount_id;

  /**
   * @var string
   */
  protected $discount_nature;

  /**
   * @var float
   */
  protected $discount_value;

  /**
   * DiscountBase constructor.
   *
   * @param string $discount_id
   * @param string $discount_nature
   * @param float $discount_value
   *
   */
  public function __construct(string $discount_id, string $discount_nature,
                              float $discount_value) {

    // Set the initial ID value for each discount.
    $this->discount_id = $discount_id;
    $this->discount_nature = $discount_nature;
    $this->discount_value = $discount_value;
  }

  /**
   * @inheritDoc
   */
  public function getDiscountID()
  {
    return $this->discount_id;
  }

  /**
   * @inheritDoc
   */
  public function setDiscountID($discount_id)
  {
    return $this->discount_id = $discount_id;
  }

  /**
   * @inheritDoc
   */
  public function getNatureOfDiscount()
  {
    return $this->discount_nature;
  }

  /**
   * @inheritDoc
   */
  public function setNatureOfDiscount($discount_nature)
  {
    $this->discount_nature = $discount_nature;
  }

  /**
   * @inheritDoc
   */
  public function changeNature()
  {
    if ($this->discount_nature == 'absolute') {
      $this->discount_nature = 'percentage';
    }else {
      $this->discount_nature = 'absolute';
    }
  }

  /**
   * @inheritDoc
   */
  public function setValue($discount_value)
  {
    $this->discount_value = $discount_value;
  }

  /**
   * @inheritDoc
   */
  public function getValue()
  {
    return $this->discount_value;
  }
}