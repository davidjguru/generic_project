<?php

namespace generic_project\Product;

/**
 * Defines an interface for products.
 */

interface ProductInterface {

  /**
   * Set a new name for Product.
   *
   * @param string $product_name
   *  New Product Name.
   *
   * @return void
   */
  public function setProductName($product_name);

  /**
   * Get the current name of a product.
   *
   * @return string
   *  Current name of a certain product.
   */
  public function getProductName();

  /**
   * Set a new ID of the product.
   *
   * @param string $product_id
   *  New ID for a product.
   *
   * @return void
   */
  public function setProductID($product_id);

  /**
   * Get the current ID of the product.
   *
   * @return string
   *  Current identifier of a product.
   */
  public function getProductID();

  /**
   * Get the current price of a product.
   *
   * @return float
   *  Current Price of the product.
   */
  public function getProductPrice();

  /**
   * Set a new price for a product.
   *
   * @param float
   *  New price for a product.
   *
   * @return void
   */
  public function setProductPrice($product_price);

  /**
   * Set a new discount for a product using nature and value.
   *
   * @param string $discount_id
   * @param string $discount_nature
   *  Nature of the discount.
   * @param float $discount_value
   *  Value of the discount.
   *
   * @return void
   */
  public function setProductDiscountByData($discount_id, $discount_nature, $discount_value);

  /**
   * Get the current Stock for a certain product.
   *
   * @return int
   *  Existing amount of a certain product.
   */
  public function getProductStock();
}