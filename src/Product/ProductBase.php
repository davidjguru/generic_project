<?php

namespace generic_project\Product;

use generic_project\Discount\DiscountBase;
use SplSubject;
use SplObserver;
use generic_project\Discount\DiscountInterface;

/**
 * Class ProductBase.
 * Define the base model for a single item.
 */
class ProductBase implements ProductInterface, SplSubject {

  /**
   * @var string
   */
  protected $product_name;

  /**
   * @var string
   */
  protected $product_id;

  /**
   * @var float
   */
  protected $product_price;

  /**
   * @var \generic_project\Discount\DiscountInterface
   */
  protected $product_discount;

  /**
   * @var int
   */
  protected $product_stock;

  /**
   * ProductBase constructor.
   *
   * @param string $product_name
   * @param string $product_id
   * @param float $product_price
   * @param \generic_project\Discount\DiscountInterface $product_discount
   * @param int $product_stock
   *
   */
  public function __construct(string $product_name, string $product_id, float $product_price,
                              \generic_project\Discount\DiscountInterface $product_discount,
                              int $product_stock)
  {

    // Set the initial ID value for each product.
    $this->product_name = $product_name;
    $this->product_id = $product_id;
    $this->product_price = $product_price;
    $this->product_discount = $product_discount;
    $this->product_stock = $product_stock;
  }


  /**
   * @inheritDoc
   */
  public function setProductName($product_name)
  {
    $this->product_name = $product_name;
  }

  /**
   * @inheritDoc
   */
  public function getProductName()
  {
    return $this->product_name;
  }

  /**
   * @inheritDoc
   */
  public function setProductID($product_id)
  {
    $this->product_id = $product_id;
  }

  /**
   * @inheritDoc
   */
  public function getProductID()
  {
    return $this->product_id;
  }

  /**
   * @inheritDoc
   */
  public function getProductPrice()
  {
    return $this->product_price;
  }

  /**
   * @inheritDoc
   */
  public function setProductPrice($product_price)
  {
    $this->product_price = $product_price;
  }

  /**
   * @inheritDoc
   */
  public function setProductDiscountByData($discount_id, $discount_nature,
                                           $discount_value)
  {
    $this->product_discount = new DiscountBase($discount_id, $discount_nature,
                                               $discount_value);
  }

  /**
   * @inheritDoc
   */
  public function getProductStock()
  {
    return $this->product_stock;
  }

  /**
   * @inheritDoc
   */
  public function attach(SplObserver $observer)
  {
    // TODO: Implement attach() method.
  }

  /**
   * @inheritDoc
   */
  public function detach(SplObserver $observer)
  {
    // TODO: Implement detach() method.
  }

  /**
   * @inheritDoc
   */
  public function notify()
  {
    // TODO: Implement notify() method.
  }
}